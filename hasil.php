<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Ini Hasilnya!</title>
    <style>
        body{background-color: white;}
        h2{color : slateblue;}
    </style>
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 border border-primary mt-4 p-3">
                 <h2 align="center">Hasil Pengolahan Form</h2> 
                    <?php
                        $nama = $_POST['nama'];
                        $mapel = $_POST['mapel'];
                        $tugas = $_POST['tugas'];
                        $uts = $_POST['uts'];
                        $uas = $_POST['uas'];
                    
                        $total = ($_POST['tugas'] * 0.15) + ($_POST['uts'] * 0.35) + ($_POST['uas'] * 0.5);
                    
                            if ($total >= 90 && $total <= 100) {
                                $grade = "A";
                            }elseif ($total > 70 && $total < 90) {
                                $grade = "B";
                            }elseif ($total > 50 && $total <= 70) {
                                $grade = "C";
                            }elseif ($total <= 50) {
                                $grade = "D";
                            } 
                        echo "Nama Siswa : $nama <br>"; 
                        echo "Mata Pelajaran  : $mapel <br>";
                        echo "Nilai Tugas : $tugas <br>";
                        echo "Nilai UTS : $uts <br>";
                        echo "Nilai UAS : $uas <br>";
                        echo "<mark>Total Nilai : $total </mark> <br>";
                        echo "Grade Nilai Siswa : $grade <br>";

                    ?>
            </div>
        </div>  
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

  </body>
</html>