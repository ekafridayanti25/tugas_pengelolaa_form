
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Pengelolaan Form</title>
    <style>
        h1{color : slateblue;}
    </style>
  </head>
  <body>
  <br>
  <h1 align="center">FORM NILAI SISWA</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 border border-primary mt-4 p-3">
                <form action="hasil.php" method="post">
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama Siswa</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                    </div>
                    <div class="mb-3">
                        <label for="mapel" class="form-label">Mata Pelajaran</label>
                        <input type="text" name="mapel" class="form-control" id="mapel">
                    </div>
                    <div class="row mb-3">
                        <label for="nilaitugas" class="col-sm-2 col-form-label">Nilai Tugas</label>
                        <div class="col-sm-6">
                            <input type="number" name="tugas" class="form-control" id="nilaitugas" placeholder="1-100">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="nilaiuts" class="col-sm-2 col-form-label">Nilai UTS</label>
                        <div class="col-sm-6">
                            <input type="number" name="uts" class="form-control" id="nilaiuts" placeholder="1-100">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="nilaiuas" class="col-sm-2 col-form-label">Nilai UAS</label>
                        <div class="col-sm-6">
                            <input type="number" name="uas" class="form-control" id="nilaiuas" placeholder="1-100">
                        </div>
                    </div>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary" >Submit</button>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

  </body>
</html>